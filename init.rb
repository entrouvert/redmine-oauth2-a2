require 'redmine'
require_dependency 'redmine_oauth2_a2/hooks'

Redmine::Plugin.register :redmine_oauth2_a2 do
  name 'Redmine Oauth Authentic plugin'
  author 'Serghei Mihai'
  description 'Plugin for authentication through Authentic'
  version '0.0.1'
  url ''
  author_url ''

  settings :default => {
    :client_id => "",
    :client_secret => "",
    :oauth_autentification => false,
    :a2_server_url => "",
  }, :partial => 'settings/oauth_settings'
end
