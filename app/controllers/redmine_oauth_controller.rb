require 'account_controller'
require 'json'
require 'oauth2'

class RedmineOauthController < AccountController
  include Helpers::MailHelper
  include Helpers::Checker
  def oauth_a2
    if Setting.plugin_redmine_oauth2_a2[:oauth_authentification]
      session[:back_url] = params[:back_url]
      redirect_to oauth_client.auth_code.authorize_url(:redirect_uri => oauth_a2_callback_url, :scope => 'read write')
    else
      password_authentication
    end
  end

  def oauth_a2_callback
    if params[:error]
      flash[:error] = l(:notice_access_denied)
      redirect_to signin_path
    else
      token = oauth_client.auth_code.get_token(params[:code], :redirect_uri => oauth_a2_callback_url)
      result = token.get(Setting.plugin_redmine_oauth2_a2[:a2_server_url] + '/idp/oauth2/user-info/')
      info = JSON.parse(result.body)
      if info
          try_to_login info
      else
        flash[:error] = l(:notice_unable_to_obtain_a2_credentials)
        redirect_to signin_path
      end
    end
  end

  def try_to_login info
   params[:back_url] = session[:back_url]
   session.delete(:back_url)
   user = User.find_or_initialize_by_mail(info["email"])
    if user.new_record?
      # Self-registration off
      redirect_to(home_url) && return unless Setting.self_registration?
      # Create on the fly
      user.firstname, user.lastname = info["name"].split(' ') unless info['name'].nil?
      user.firstname ||= info[:given_name]
      user.lastname ||= info[:family_name]
      user.mail = info["email"]
      user.login = parse_email(info["email"])[:login]
      user.login ||= [user.firstname, user.lastname]*"."
      user.random_password
      user.register

      case Setting.self_registration
      when '1'
        register_by_email_activation(user) do
          onthefly_creation_failed(user)
        end
      when '3'
        register_automatically(user) do
          onthefly_creation_failed(user)
        end
      else
        register_manually_by_administrator(user) do
          onthefly_creation_failed(user)
        end
      end
    else
      # Existing record
      if user.active?
        successful_authentication(user)
      else
        account_pending
      end
    end
  end

  def oauth_client
    @client ||= OAuth2::Client.new(settings[:client_id], settings[:client_secret],
      :site => settings[:a2_server_url],
      :authorize_url => '/idp/oauth2/authorize/',
      :token_url => '/idp/oauth2/access_token/'
      )
  end

  def settings
    @settings ||= Setting.plugin_redmine_oauth2_a2
  end

  def scopes
    Setting.plugin_redmine_oauth2_a2[:a2_scopes_urls]
  end
end
